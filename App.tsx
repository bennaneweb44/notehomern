import Navigation from './Navigation';
import { Provider } from 'react-redux';
import { store } from '@/store';
import { PersistGate } from 'redux-persist/integration/react';
import { persistor } from '@/store';

export default function App() {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <Navigation />
      </PersistGate>
    </Provider>
  )
}
