import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { ShowToastProvider } from '@/providers/ShowToastProvider';
import { ToastProvider } from 'react-native-toast-notifications';
import {
  LoginScreen,
  RegisterScreen,
  ResetPasswordScreen,
  NotesListScreen,
  CategoriesListScreen,
  RayonsListScreen,
  LoadingScreen,
  ProfilScreen,
} from '@/screens';
import { Text } from 'react-native';
import { useAppSelector } from '@/hooks';
import { RootState } from '@/store';

const Tab = createBottomTabNavigator()

export default function Navigation() {
  const auth = useAppSelector((state: RootState) => state.auth);
  const isLoading = false;

  return (
    <ToastProvider>
      <ShowToastProvider>
        <NavigationContainer>
          <Tab.Navigator 
            screenOptions={({ route }) => ({
              headerShown: false,
              tabBarLabelStyle: {fontSize: 14},
              tabBarIcon: ({ focused, color, size }) => {
                let icon;

                switch (route.name) {
                  case 'Connexion' :
                    icon = '🔓';
                    break;
                  case 'Inscription' :
                    icon = '✒️';
                    break;
                  case 'Mot de passe oublié' :
                    icon = '🏠';
                    break;
                  case 'Notes' :
                    icon = '📝';
                    break;
                  case 'Catégories' :
                    icon = '♻️';
                    break;
                  case 'Rayons' :
                    icon = '🛍️';
                    break;
                  case 'Profil' :
                    icon = '👨';
                    break;
                }

                // You can return any component that you like here!
                return <Text>{icon}</Text>
              },
              tabBarActiveTintColor: 'purple',
              tabBarInactiveTintColor: 'gray',
            })}
          >
            {isLoading ?
              <Tab.Screen name="LoadingScreen" component={LoadingScreen} />
              :
              auth.token ? (
                <>
                  <Tab.Screen name="Notes" component={NotesListScreen} options={{ tabBarBadge: 1 }} />
                  <Tab.Screen name="Catégories" component={CategoriesListScreen} />
                  <Tab.Screen name="Rayons" component={RayonsListScreen} />
                  <Tab.Screen name="Profil" component={ProfilScreen} />
                </>
              ) : (
                <>
                  <Tab.Screen name="Connexion" component={LoginScreen} />
                  <Tab.Screen name="Inscription" component={RegisterScreen} />
                  <Tab.Screen name="Mot de passe oublié" component={ResetPasswordScreen} />
                </>
              )
            }
          </Tab.Navigator>
        </NavigationContainer>
      </ShowToastProvider>
    </ToastProvider>
  )
}
