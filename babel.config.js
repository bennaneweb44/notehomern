/** @type {import('ts-jest').JestConfigWithTsJest} */
module.exports = function (api) {
  api.cache(true);
  return {
    presets: ['@react-native/babel-preset'],
    plugins: [
      ["@babel/plugin-proposal-class-properties", { loose: true}],
      ["@babel/plugin-proposal-private-methods", { loose: true}],
      ['@babel/plugin-transform-private-methods', { loose: true}],
      ['@babel/plugin-transform-private-property-in-object', { loose: true}],
      ['transform-inline-environment-variables', {
        include: ['EXPO_OS'],
      }],
    ]
  }
}
