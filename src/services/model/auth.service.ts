import { AxiosResponse } from 'axios'
import { requestPublic } from '@/services/http/http'
import { Auth } from 'AuthModel'

const login = async (
  username: string,
  password: string
): Promise<AxiosResponse<Auth>> =>
  requestPublic
    .post('/login', {
      username: `${username}`,
      password: `${password}`,
    })
    .catch((err: any) => Promise.reject(err))

const register = async (
  username: string,
  email: string,
  password: string
): Promise<AxiosResponse<Auth>> =>
  requestPublic
    .post('/register', {
      username: `${username}`,
      email: `${email}`,
      password: `${password}`,
    })
    .catch((err: any) => Promise.reject(err))

const passwordForget = async (
  email: string,
): Promise<AxiosResponse<Auth>> =>
  requestPublic
    .post('/password/forget', {
      email: `${email}`,
    })
    .catch((err: any) => Promise.reject(err))

export const authService = {
  login,
  register,
  passwordForget,
}
