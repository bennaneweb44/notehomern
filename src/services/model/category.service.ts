import { AxiosResponse } from 'axios';
import { requestPublic } from '@/services/http/http';
import { IApiCategory } from '@/types/api.interfaces';

const getAll = async (): Promise<AxiosResponse<IApiCategory[]>> =>
  requestPublic
    .get(`/api/categories`)
    .catch((err: any) => Promise.reject(err));

const getOne = async (id: number): Promise<AxiosResponse<IApiCategory>> =>
  requestPublic
    .get(`/api/categories/${id}`)
    .catch((err: any) => Promise.reject(err));

const update = async (id: number, payload: Partial<IApiCategory>) : Promise<AxiosResponse<IApiCategory>> =>
  requestPublic
    .patch(`/api/categories/${id}`, payload, { headers: {
      'Content-Type': 'application/merge-patch+json'
    }})
    .catch((err: any) => Promise.reject(err));

export const categoryService = {
  getAll,
  getOne,
  update,
}
