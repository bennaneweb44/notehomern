import { AxiosResponse } from 'axios'
import { requestPublic, requestUploadImage } from '@/services/http/http'
import { IUser } from 'UserModel'

const getOne = async (id: number): Promise<AxiosResponse<IUser>> =>
  requestPublic
    .get(`/api/users/${id}`)
    .catch((err: any) => Promise.reject(err))

const uploadImage = async (id: number, formData: FormData): Promise<AxiosResponse<IUser>> =>
  requestUploadImage
    .postForm(`/api/users/${id}/upload/avatar`, formData)
    .catch((err: any) => Promise.reject(err))

const update = async (id: number, payload: Partial<IUser>) : Promise<AxiosResponse<IUser>> =>
  requestPublic
    .patch(`/api/users/${id}`, payload, { headers: {
      'Content-Type': 'application/merge-patch+json'
    }})
    .catch((err: any) => Promise.reject(err))

export const userService = {
  getOne,
  update,
  uploadImage,
}
