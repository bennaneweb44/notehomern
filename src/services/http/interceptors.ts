import {
  AxiosError,
  AxiosInstance,
  AxiosResponse,
  HttpStatusCode,
  InternalAxiosRequestConfig
} from 'axios';
import { resetAuth } from '@/store/authentication';
import { store } from '@/store';

const onRequest = async (config: InternalAxiosRequestConfig): Promise<InternalAxiosRequestConfig> => {
  const state = store.getState();
  const token = state.auth.token;
  if (token && config && config.headers) {
    config.headers['Authorization'] = `Bearer ${token}`;
  }
  config.headers['Accept'] = `application/json`;
  return config;
}

const onRequestError = (error: AxiosError): Promise<AxiosError> => {
  return Promise.reject(error);
}

const onResponse = (response: AxiosResponse): AxiosResponse => {
  return response;
}

const onResponseError = async (error: AxiosError): Promise<AxiosError> => {
  if (error.response?.status === HttpStatusCode.Unauthorized) {
    store.dispatch(resetAuth(null));
  }
  return Promise.reject(error);
}

export function setupInterceptorsTo(axiosInstance: AxiosInstance): AxiosInstance {
  axiosInstance.interceptors.request.use(onRequest, onRequestError);
  axiosInstance.interceptors.response.use(onResponse, onResponseError);
  return axiosInstance;
}
