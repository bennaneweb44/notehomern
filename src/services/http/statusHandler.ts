/**
 * Status Handler - For handling network responses
 */
import { AxiosError, HttpStatusCode } from 'axios'

const statusHandler = (err: AxiosError) => {
  if (err.response) {
    switch (err.response.status) {
      case HttpStatusCode.Unauthorized: {
        //TODO: refresh token : implement requestNewToken()
        break
      }
      default: {
        //
      }
    }
  }
}

export default statusHandler
