import axios from 'axios';
import { BACKEND_DEFAULT_CONTENT_TYPE } from '@/common/Constants';
import { setupInterceptorsTo } from '@/services/http/interceptors';

const requestPublic = axios.create({
  baseURL: `${process.env.EXPO_PUBLIC_BACKEND_BASE_URL}`,
  headers: {
    'Content-Type': BACKEND_DEFAULT_CONTENT_TYPE
  }
});

const requestUploadImage = axios.create({
  baseURL: `${process.env.EXPO_PUBLIC_BACKEND_BASE_URL}`,
  headers: {
    'Content-Type': 'multipart/form-data',
    'Cache-Control': 'no-store',
  }
});

setupInterceptorsTo(requestPublic);
setupInterceptorsTo(requestUploadImage);

export { requestPublic, requestUploadImage }
