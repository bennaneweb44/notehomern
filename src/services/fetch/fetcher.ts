import { store } from '@/store';

const fetcher = async (url: string) => {
  const state = store.getState();
  const token = state.auth.token;

  const response = await fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `bearer ${token}`,
    },
  });

  if (response.status !== 200 && response.status !== 201) {
    let message = 'Une erreur interne s\'est produite';
    if (response.status === 401) {
      message = 'Session expirée, veuillez vous se reconnecter !';
    }
    const error = new Error(message);
    throw error;
  }

  return response.json();
};

export default fetcher;
