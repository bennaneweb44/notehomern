import { MSG_EMAIL_CANT_BE_EMPTY, MSG_INVALID_EMAIL } from "@/common/Constants"

export function emailValidator(email: string) {
  const re = /\S+@\S+\.\S+/;
  if (!email) return MSG_EMAIL_CANT_BE_EMPTY;
  if (!re.test(email)) return MSG_INVALID_EMAIL;
  return '';
}
