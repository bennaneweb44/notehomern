import { MSG_INVALID_PASSWORD, MSG_PASSWORD_CANT_BE_EMPTY } from "@/common/Constants"

export function passwordValidator(password: string) {
  if (!password) return MSG_PASSWORD_CANT_BE_EMPTY
  if (password.length < 6) return MSG_INVALID_PASSWORD
  return ''
}
