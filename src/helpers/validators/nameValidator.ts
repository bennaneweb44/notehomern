import { MSG_NAME_CANT_BE_EMPTY } from "@/common/Constants"

export function nameValidator(name: string) {
  if (!name || '' === name.trim()) return MSG_NAME_CANT_BE_EMPTY;
  return '';
}
