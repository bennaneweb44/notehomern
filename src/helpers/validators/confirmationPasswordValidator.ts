import { MSG_PASSWORD_NOT_CONFIRMED } from "@/common/Constants";

export function confirmationPasswordValidator(password: string, confirmation: string) {
  return password.trim() === confirmation.trim() ? '' : MSG_PASSWORD_NOT_CONFIRMED;
}
