import { createSlice, Slice } from '@reduxjs/toolkit';
import { IAuth } from 'AuthModel';

// Initial state
const initialState: IAuth = {
  id: 0,
  token: null
};

// Slice
export const authSlice: Slice = createSlice({
  name: 'auth',
  initialState: initialState,
  reducers: {
    setAuth: (state: IAuth, action: any) => {
      state.id = action.payload.id;
      state.token = action.payload.token;
    },
    resetAuth: (state: IAuth) => {
      state.id = initialState.id;
      state.token = initialState.token;
    },
  },
});

// Methods
export const { setAuth, resetAuth } = authSlice.actions;
export const authReducer = authSlice.reducer;
