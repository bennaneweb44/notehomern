// URI
export const BACKEND_DEFAULT_CONTENT_TYPE = 'application/json';

// MESSAGES : AUTH
export const MSG_GOOD_AUTH = 'Connecté(e) avec succès.';
export const MSG_BAD_AUTH = 'Identifiants invalides';
export const MGS_ERROR_AUTH = 'Erreur interne de connexion ...';
export const MSG_DISCONNECTED = 'Déconnecté(e) avec succès';
export const MSG_PASSWORD_NOT_CONFIRMED = 'La mot de passe n\'est pas bien confirmé';

// MESSAGES : REGISTER
export const MGS_ERROR_REGISTER = 'Erreur interne lors de l\'inscription ...';
export const MGS_ERROR_PASSWORD_FORGET = 'Erreur interne lors de la demande de réinitialation ...';

// MESSAGES : VALIDATION
export const MSG_EMAIL_CANT_BE_EMPTY = 'L\'email ne peut être vide.';
export const MSG_NAME_CANT_BE_EMPTY = 'Le nom d\'utilisateur ne peut être vide.';
export const MSG_PASSWORD_CANT_BE_EMPTY = 'Le mot de passe ne peut être vide.';
export const MSG_INVALID_EMAIL = 'Adresse email invalide.';
export const MSG_INVALID_PASSWORD = 'Le mot de passe doit contenir au moins 6 caractères.';

// TOAST
export enum ToastEnum {
    SUCCESS = "success",
    ERROR = "danger",
};

// AUTH
export const defaultAuthObject = {
    value: '',
    error: '',
};
