import { useState, PropsWithChildren } from "react";
import { ToastContext } from "@/context/ToastContext";
import { ToastEnum } from "@/common/Constants";
import { Toast } from "@/components/common/Toast";
import { IToast } from "@/types/app.interfaces";

export const ShowToastProvider = ({ children }: PropsWithChildren) => {
    const [toast, setToast] = useState<{
        message: string;
        type: ToastEnum;
    } | null>(null)

    const showToast = ({ message, type = ToastEnum.SUCCESS }: IToast) => {
        setToast({ message, type })

        setTimeout(() => {
            setToast(null)
        }, 2000)
    }

    return (
        <ToastContext.Provider value={{ showToast }}>
            {children}
            {toast && <Toast message={toast.message} type={toast.type} />}
        </ToastContext.Provider>
    )
}
