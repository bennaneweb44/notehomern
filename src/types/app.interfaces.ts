import { ToastEnum } from "../common/Constants";

// TOAST
export interface IToast {
  message: string;
  type: ToastEnum;
}

export interface ILoader {
  size: number;
  overlayColor: string;
  textContent: string;
}

export interface ToastContextType {
  showToast: (props: IToast) => void;
}

// ICONS
export interface IAppIcons {
  name: string;
  size: number;
  color: string;
  style: object;
}

// CATEGORIES
export interface IAppCategory {
  id: number;
  nom: string;
  couleur: string;
  icone: IAppIcons;
}
