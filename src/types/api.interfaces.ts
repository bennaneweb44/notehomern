// CATEGORIES
export interface IApiCategory {
  id: number;
  nom: string;
  couleur: string;
  icone: string;
}
