import { StyleSheet } from "react-native"

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: 'auto',
    padding: 25,
  },
  input: {
    flex: 1,
    backgroundColor: "#ddd",
    height: 44,
    borderRadius: 6,
    color: "#000",
    padding: 6,
    fontSize: 16,
    marginRight: 12,
  },
  button: {
    width: 44,
    height: 44,
    borderRadius: 6,
    backgroundColor: "#1E6F9F",
    alignItems: "center",
    justifyContent: "center",
  },
  form: {
    width: "100%",
    flexDirection: "row",
    marginTop: 6,
    marginBottom: -10,
  },
  titleContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 32,
    marginBottom: 24,
  },
  listSeparator: {
    margin: 4,
  },
})
