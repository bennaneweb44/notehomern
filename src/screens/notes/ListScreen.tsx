import { useState } from "react"
import {
  Keyboard,
  View,
  TextInput,
  TouchableOpacity,
  FlatList,
  Alert,
  Image,
  ImageBackground,
} from "react-native"
import { NoteItem } from "@/components/notes/NoteItem"
import { EmptyList } from "@/components/notes/EmptyList"
import { NoteInfo } from "@/components/notes/NoteInfo"

import plus from "@/assets/plus.png";
import { styles } from "./styles"
import Header from "@/components/ui/Header";

export type TaskProps = {
  id: string;
  title: string;
  checked: boolean;
};

export default function ListScreen() {
  const [tasks, setTasks] = useState<TaskProps[]>([])
  const [taskTitle, setTaskTitle] = useState("")

  function handleAddTask() {
    if (!taskTitle) {
      return Alert.alert("Atenção", "Informe o nome da tarefa.");
    }

    const newTask = {
      id: new Date().getTime().toString(),
      title: taskTitle,
      checked: false,
    };

    setTasks((prev) => [...prev, { ...newTask }]);
    setTaskTitle("");
    Keyboard.dismiss();
  }

  function handleCheck(id: string) {
    const newTasks = tasks.map((item) => {
      if (item.id === id) item.checked = !item.checked;
      return item;
    });

    setTasks(newTasks);
  }

  function handleRemoveTask(id: string) {
    Alert.alert("Suppression", `Voulez-vous vraiment retirer cet élément ?`, [
      { text: "Non", style: "cancel" },
      {
        text: "Oui",
        onPress: () => {
          const newTasks = tasks.filter((item) => item.id !== id);
          setTasks(newTasks);
        },
      },
    ]);
  }

  return (
    <ImageBackground
      source={require('../../../assets/bg.jpg')}
      resizeMode="cover"
      style={styles.container}
    >
      <Header>📝 Notes</Header>
      <View style={styles.form}>
        <TextInput
          style={styles.input}
          placeholder="Tapez une nouvelle note ici"
          placeholderTextColor="#808080"
          value={taskTitle}
          onChangeText={setTaskTitle}
        />
        <View>
          <TouchableOpacity style={styles.button} onPress={handleAddTask}>
            <Image source={plus} />
          </TouchableOpacity>
        </View>
      </View>

      <View style={styles.titleContainer}>
        <NoteInfo label="Total" value={tasks.length} color="#6218AB" />

        <NoteInfo
          label="Faites"
          value={tasks.filter((item) => item.checked).length}
          color="#1E90C1"
        />
      </View>

      <FlatList
        data={tasks}
        renderItem={({ item }) => (
          <NoteItem
            key={item.id}
            data={item}
            handleCheck={handleCheck}
            handleDelete={handleRemoveTask}
          />
        )}
        showsVerticalScrollIndicator={false}
        ListEmptyComponent={() => <EmptyList />}
      />
    </ImageBackground>
  );
}