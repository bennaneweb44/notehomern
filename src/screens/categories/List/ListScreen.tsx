import React, { useEffect, useState } from 'react';
import ListCategories from "@/components/categories/List/ListCategories";
import fetcher from "@/services/fetch/fetcher";
import { useToast } from '@/context/ToastContext';
import { ActivityIndicator } from "react-native";
import useSWR from 'swr';
import { useAppDispatch } from '@/hooks';
import { resetAuth } from '@/store/authentication';
import { useIsFocused } from '@react-navigation/native';
import { ToastEnum } from '@/common/Constants';

export type CategoryProps = {
  id: string;
  title: string;
  checked: boolean;
};

const ListScreen = () => {
  const { showToast } = useToast();
  const { data, isLoading, mutate } = useSWR(`${process.env.EXPO_PUBLIC_BACKEND_BASE_URL}/api/categories`, fetcher, {
    onError: (error) => {
      dispatch(resetAuth(null));
      showToast({
        message: error.message,
        type: ToastEnum.ERROR,
      });
    },
  });
  const [categories, setCategories] = useState([]);
  const dispatch = useAppDispatch();
  const isFocused = useIsFocused();
  
  useEffect(() => {
    if (data) {
      setCategories(data['hydra:member']);
    }
  }, [data])

  useEffect(() => {
    if (isFocused) {
      mutate();
    }
  }, [isFocused, mutate]);

  if (isLoading) return <ActivityIndicator size="large" color="#0000ff" />;

  return (
    <>
      {categories && <ListCategories categories={categories} />}
    </>
  );
}

export default ListScreen;
