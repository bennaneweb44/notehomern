import React from 'react'

// Components
import Background from '../components/ui/Background'
import Header from '../components/ui/Header'

export default function LoadingScreen() {

  return (
    <Background>
      <Header> Chargement ...</Header>
    </Background>
  )
}
