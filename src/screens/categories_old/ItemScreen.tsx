import React from 'react';
import {
  Text,
  View,
  TouchableOpacity,
} from 'react-native';
import { styles } from './styles';
import { Icon } from '@/components/ui/Icon';

const ItemScreen = ({ ...props }) => {
  const { item, showModal } = props;

  return (
    <TouchableOpacity
      style={[styles.card, { backgroundColor: item.couleur }]}
      onPress={() => showModal(item) }>
      <View style={styles.cardHeader}>
        <Text style={styles.title}>{item.nom}</Text>
      </View>
      <Icon style={styles.cardIcon} name={item.icone} size={60} color='#333' />
      <View style={styles.cardFooter}>
        <Text style={styles.subTitle}>{item.notes.length} notes</Text>
      </View>
    </TouchableOpacity>
  )
}

export default ItemScreen;
