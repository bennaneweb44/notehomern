import React from 'react';
import { styles } from './styles';
import Modal from 'react-native-modal';
import CategoryForm from '@/components/forms/CategoryForm';
import { IApiCategory } from '@/types/api.interfaces';
import { categoryService } from '@/services/model/category.service';


const FormScreen = ({ ...props }) => {
  const { item, setShow } = props;

  const updateCategory = (item: IApiCategory) => {
    console.log(`updated category ${item.couleur} ${item.icone} ${item.nom}`)
    categoryService
      .update(item.id, {nom: item.nom, icone: item.icone, couleur: item.couleur})
      .then((resp) => {
        if (resp.data) {
          setShow(false);
          //navigation.navigate('Catégories');
        }
      })
      .catch((error) => {
        console.error(error);
      });
  };

  return (
    <Modal isVisible={true} style={styles.container}>
      <CategoryForm item={item} setShow={setShow} updateCategory={updateCategory} />
    </Modal>
  )
}

export default FormScreen;
