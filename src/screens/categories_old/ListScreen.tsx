import React, { useCallback, useEffect, useState } from 'react';
import {
  Text,
  View,
  FlatList,
} from 'react-native';

import { styles } from './styles';
import { categoryService } from '@/services/model/category.service';
import { IApiCategory } from '@/types/api.interfaces';
import { useToast } from '@/context/ToastContext';
import { ToastEnum } from '@/common/Constants';
import { useNavigation } from '@react-navigation/native';
import { Loader } from '@/components/common/Loader';
import ItemScreen from './ItemScreen';
import FormScreen from './FormScreen';

const ListScreen = () => {
  const { showToast } = useToast();
  const [data, setData] = useState<IApiCategory[]>([]);
  const [showSpinner, setShowSpinner] = useState(true);
  const [show, setShow] = useState(false);
  const [selectedItem, setSelectedItem] = useState<IApiCategory|null>(null);
  const navigation = useNavigation();

  useEffect(() => {
    setShowSpinner(true);
    const unsubscribe = navigation.addListener('focus', () => {
      // The screen is focused
      setTimeout(() => {
        fetchCategories();
      }, 300);
    });

    // Return the function to unsubscribe from the event so it gets removed on unmount
    return unsubscribe;
  }, [navigation]);

  useEffect(() => {
    if (show === false) {
      fetchCategories();
    }
  }, [show]);

  const fetchCategories = () => {
    categoryService
      .getAll()
      .then((resp) => {
        if (resp.data) {
          setData(resp.data);
        }
      })
      .catch((error) => {
        if (error.response?.status === 500) {
          showToast({
            message: 'Une erreur interne s\'est produite',
            type: ToastEnum.ERROR,
          });
        }
      })
      .finally(() => {
        setShowSpinner(false);
      });
  };

  const showModal = useCallback((item: IApiCategory) => {
    setShow(true);
    setSelectedItem(item);
  }, []);

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Catégories</Text>
      {show && 
        <FormScreen item={selectedItem} setShow={setShow} />
      }
      {data.length || !showSpinner ?
        <FlatList
          style={styles.list}
          contentContainerStyle={styles.listContainer}
          data={data}
          horizontal={false}
          numColumns={2}
          keyExtractor={(item: any) => {
            return item.id
          }}
          renderItem={({ item }: any) => <ItemScreen item={item} showModal={showModal} />}
        />
        :
        <Loader size={47} overlayColor='whitesmoke' textContent='Chargement ...' />
      }
    </View>
  )
}

export default ListScreen;
