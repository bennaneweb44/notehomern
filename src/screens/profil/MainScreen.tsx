import React, { useEffect, useState } from 'react';
// Components
import Background from '@/components/ui/Background';
import {
  defaultAuthObject,
  MSG_DISCONNECTED,
  ToastEnum
} from '@/common/Constants';
import { useToast } from '@/context/ToastContext';
import Header from '@/components/ui/Header';
import { Text } from 'react-native';
import * as ImagePicker from 'expo-image-picker';
import * as FileSystem from 'expo-file-system';
import Disconnect from '@/components/auth/Disconnect';
import { userService } from '@/services/model/user.service';
import { HttpStatusCode } from 'axios';
import ProfilForm from '@/components/forms/ProfilForm';
import { resetAuth } from '@/store/authentication';
import { useAppDispatch, useAppSelector } from '@/hooks';
import { RootState } from '@/store';

export default function MainScreen() {
  const { showToast } = useToast();
  const [loading, setLoading] = useState(false);
  const [username, setUsername] = useState(defaultAuthObject);
  const [userProfileName, setUserProfileName] = useState('');
  const [email, setEmail] = useState(defaultAuthObject);
  const [avatar, setAvatar] = useState('');
  const auth = useAppSelector((state: RootState) => state.auth);
  const dispatch = useAppDispatch();

  useEffect( () => {
    setLoading(true);

    userService
      .getOne(auth.id)
      .then((resp) => {
        if (resp.data) {
          setUserProfileName(resp.data.username)
          setUsername({
            value: resp.data.username,
            error: '',
          });
          setEmail({
            value: resp.data.email,
            error: '',
          });
          const currentTime = new Date().getTime();
          if (resp.data.avatar !== undefined) {
            setAvatar(`${process.env.EXPO_PUBLIC_BACKEND_BASE_URL}/images/users/${resp.data.avatar}?${currentTime}`);
          } else {
            setAvatar('../../../assets/user.jpg');
          }
        }
      })
      .catch((error) => {
        if (error.response?.status === 500) {
          showToast({
            message: 'Une erreur interne s\'est produite',
            type: ToastEnum.ERROR,
          });
        }
      })
      .finally(() => {
        setLoading(false);
      });
    
  }, []);

  const pickImageAsync = async () => {

    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    })

    if (!result.canceled) {
      setLoading(true);

      const uriParts = result.assets[0].uri.split('.');
      const fileType = uriParts[uriParts.length - 1];

      // Check max size
      const fileInfo = await FileSystem.getInfoAsync(result.assets[0].uri);
      if (fileInfo.exists && fileInfo.size > 1000000) {
        showToast({
          message: 'Ce fichier est trop volumineux (max: 1Mo)',
          type: ToastEnum.ERROR,
        })

        setLoading(false);
        return;
      }

      const formData = new FormData();
      formData.append('file', {
        uri: result.assets[0].uri,
        name: `photo.${fileType}`,
        type: `image/${fileType}`,
      } as unknown as Blob);

      await userService
        .uploadImage(auth.id, formData)
        .then((resp) => {
          if (resp.data) {
            const currentTime = new Date().getTime()
            setAvatar(`${process.env.EXPO_PUBLIC_BACKEND_BASE_URL}/images/users/${resp.data.avatar}?${currentTime}`)
          }
        })
        .catch((error) => {
          if (error.response?.status === 500) {
            showToast({
              message: 'Une erreur interne s\'est produite',
              type: ToastEnum.ERROR,
            })
          }
        })
        .finally(() => {
          setLoading(false)
        })
    }
  }

  const onSignOutPressed = () => {
    setLoading(true)

    setTimeout(function() {
      dispatch(resetAuth(null));
      showToast({
        message: MSG_DISCONNECTED,
        type: ToastEnum.SUCCESS,
      });
    }, 500)
    
    setLoading(false)
  }

  const onProfilUpdated = async () => {
    setLoading(true)

    await userService
        .update(auth.id as number, {username: username.value, email: email.value})
        .then((resp) => {
          if (resp.data) {
            setUsername({
              value: resp.data.username,
              error: '',
            })
            setEmail({
              value: resp.data.email,
              error: '',
            })
            userProfileName !== resp.data.username && onSignOutPressed()
          }
        })
        .catch((error) => {
          let message = ''

          switch (error.response?.status) {
            case HttpStatusCode.UnprocessableEntity :
              message = 'Ce nom d\'utilisateur existe déjà'
              break
            case HttpStatusCode.InternalServerError:
              message = 'Une erreur interne s\'est produite'
              break
          }
          showToast({
            message: message,
            type: ToastEnum.ERROR,
          })
        })
        .finally(() => {
          setLoading(false)
        })
  }

  const onLoadStart = () => setLoading(true)
  const onLoadEnd = () => setLoading(false)
  const onChangeUsername = (text: string) => setUsername({ value: text, error: '' })
  const onChangeEmail = (text: string) => setEmail({ value: text, error: '' })

  return (
    <Background>

      <Disconnect onSignOutPressed={onSignOutPressed} />

      <Header>
        <Text style={{ fontStyle: 'normal' }}>Bienvenue, </Text> { userProfileName }
      </Header>
      
      <ProfilForm
        avatar={avatar}
        loading={loading}
        onLoadStart={onLoadStart}
        onLoadEnd={onLoadEnd}
        setLoading={setLoading}
        pickImageAsync={pickImageAsync}
        onProfilUpdated={onProfilUpdated}
        username={username}
        email={email}
        onChangeUsername={onChangeUsername}
        onChangeEmail={onChangeEmail}
        setUsername={setUsername}
        setEmail={setEmail}
      />

    </Background>
  )
}
