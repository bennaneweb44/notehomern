import React, { useState } from 'react';
// Components
import Background from '@/components/ui/Background';
// Internal
import { authService } from '@/services/model/auth.service';
import { 
  MGS_ERROR_AUTH,
  MSG_BAD_AUTH,
  MSG_GOOD_AUTH,
  ToastEnum
} from '@/common/Constants';
import LoginForm from '@/components/forms/LoginForm';
import { useToast } from '@/context/ToastContext';
import { setAuth } from '@/store/authentication';
import { useAppDispatch } from '@/hooks';

export default function LoginScreen({ navigation }: any) {
  const [loading, setLoading] = useState(false);
  const { showToast } = useToast();
  const dispatch = useAppDispatch();

  const onLoginPressed = async (username: string, password: string) => {
    setLoading(true)
    authService
      .login(username, password)
      .then((resp) => {
        if (resp.data) {
          const newAuth = {
            id: resp.data.id,
            token: resp.data.token
          };
          dispatch(setAuth(newAuth));
          showToast({
            message: MSG_GOOD_AUTH,
            type: ToastEnum.SUCCESS,
          });
        }
      })
      .catch((error) => {
        let _message = error.response?.status === 401 ? MSG_BAD_AUTH : MGS_ERROR_AUTH;
        showToast({
          message: _message,
          type: ToastEnum.ERROR,
        });
      })
      .finally(() => {
        setLoading(false);
      });
  }

  return (
    <Background>
      <LoginForm loading={loading} onLoginPressed={onLoginPressed} navigation={navigation} />
    </Background>
  )
}
