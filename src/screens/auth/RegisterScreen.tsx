import React, { useState } from 'react'
import { View, StyleSheet, TouchableOpacity } from 'react-native'
import { Text } from 'react-native-paper'

import Background from '@/components/ui/Background'
import Header from '@/components/ui/Header'
import Button from '@/components/ui/Button'
import TextInput from '@/components/ui/TextInput'
import BackButton from '@/components/ui/BackButton'
import { theme } from '@/core/theme'
import { emailValidator } from '@/helpers/validators/emailValidator'
import { passwordValidator } from '@/helpers/validators/passwordValidator'
import { nameValidator } from '@/helpers/validators/nameValidator'
import { defaultAuthObject, MGS_ERROR_REGISTER, ToastEnum } from '@/common/Constants'
import { confirmationPasswordValidator } from '@/helpers/validators/confirmationPasswordValidator'
import { authService } from '@/services/model/auth.service'
import { useToast } from '@/context/ToastContext'

export default function RegisterScreen({ navigation }: any) {
  const { showToast } = useToast();

  const [username, setUsername] = useState(defaultAuthObject)
  const [email, setEmail] = useState(defaultAuthObject)
  const [password, setPassword] = useState(defaultAuthObject)
  const [confirmation, setConfirmation] = useState(defaultAuthObject)
  const [loading, setLoading] = useState(false)

  const onSignUpPressed = () => {
    const usernameError = nameValidator(username.value)
    const emailError = emailValidator(email.value)
    const passwordError = passwordValidator(password.value)
    const confirmationError = confirmationPasswordValidator(password.value, confirmation.value)

    if (usernameError || emailError || passwordError || confirmationError) {
      setUsername({ ...username, error: usernameError })
      setEmail({ ...email, error: emailError })
      setPassword({ ...password, error: passwordError })
      setConfirmation({ ...confirmation, error: confirmationError })
      return
    }

    setLoading(true)
    authService
      .register(username.value, email.value, password.value)
      .then((resp) => {
        navigation.navigate('LoginScreen')
        showToast({
          message: resp.data.message,
          type: ToastEnum.SUCCESS,
        })

        navigation.reset({
          index: 0,
          routes: [{ name: 'LoginScreen' }],
        })
      })
      .catch((error) => {
        let _message = MGS_ERROR_REGISTER
        if (error.response?.status === 400 && error.response.data.errors.length > 0) {
          _message = error.response.data.errors[0]
        }

        showToast({
          message: _message,
          type: ToastEnum.ERROR,
        })
      })
      .finally(() => {
        setLoading(false)
      })
  }

  return (
    <Background>
      <BackButton goBack={navigation.goBack} />
      <Header>✒️ Inscription</Header>
      <TextInput
        label="Nom"
        returnKeyType="next"
        value={username.value}
        onChangeText={(text: string) => setUsername({ value: text, error: '' })}
        error={!!username.error}
        errorText={username.error}
      />
      <TextInput
        label="Email"
        returnKeyType="next"
        value={email.value}
        onChangeText={(text: string) => setEmail({ value: text, error: '' })}
        error={!!email.error}
        errorText={email.error}
        autoCapitalize="none"
        autoCompleteType="email"
        textContentType="emailAddress"
        keyboardType="email-address"
      />
      <TextInput
        label="Mot de passe"
        returnKeyType="done"
        value={password.value}
        onChangeText={(text: string) => setPassword({ value: text, error: '' })}
        error={!!password.error}
        errorText={password.error}
        secureTextEntry
      />
      <TextInput
        label="Confirmation"
        returnKeyType="done"
        value={confirmation.value}
        onChangeText={(text: string) => setConfirmation({ value: text, error: '' })}
        error={!!confirmation.error}
        errorText={confirmation.error}
        secureTextEntry
      />
      <Button
          mode="contained"
          onPress={onSignUpPressed}
          label="S'inscrire"
          loading={loading}
      />

      <View style={styles.row}>
        <Text>Déjà inscrit ? </Text>
        <TouchableOpacity onPress={() => navigation.replace('LoginScreen')}>
          <Text style={styles.link}>Se connecter</Text>
        </TouchableOpacity>
      </View>
    </Background>
  )
}

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    marginBottom: -90,
  },
  link: {
    fontWeight: 'bold',
    color: theme.colors.primary,
  },
})
