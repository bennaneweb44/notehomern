import React, { useState } from 'react'
import Background from '@/components/ui/Background'
import BackButton from '@/components/ui/BackButton'
import Header from '@/components/ui/Header'
import TextInput from '@/components/ui/TextInput'
import Button from '@/components/ui/Button'
import { emailValidator } from '@/helpers/validators/emailValidator'
import { defaultAuthObject, MGS_ERROR_PASSWORD_FORGET, ToastEnum } from '@/common/Constants'
import { authService } from '@/services/model/auth.service'
import { useToast } from '@/context/ToastContext'

export default function ResetPasswordScreen({ navigation }: any) {
  const { showToast } = useToast();
  const [email, setEmail] = useState(defaultAuthObject)
  const [loading, setLoading] = useState(false)

  const sendResetPasswordEmail = () => {
    const emailError = emailValidator(email.value)
    if (emailError) {
      setEmail({ ...email, error: emailError })
      return
    }

    setLoading(true)
    authService
      .passwordForget(email.value)
      .then((resp) => {
        showToast({
          message: resp.data.message,
          type: ToastEnum.SUCCESS,
        })

        navigation.reset({
          index: 0,
          routes: [{ name: 'LoginScreen' }],
        })
      })
      .catch((error) => {
        let _message = MGS_ERROR_PASSWORD_FORGET
        if (error.response?.status === 400 && error.response.data.message.length > 0) {
          _message = error.response.data.message
        }

        showToast({
          message: _message,
          type: ToastEnum.ERROR,
        })
      })
      .finally(() => {
        setLoading(false)
      })
  }

  return (
    <Background>
      <BackButton goBack={navigation.goBack} />
      <Header>🔑 Récupérer mot de passe</Header>
      <TextInput
        label="Adresse e-mail"
        returnKeyType="done"
        value={email.value}
        onChangeText={(text: string) => setEmail({ value: text, error: '' })}
        error={!!email.error}
        errorText={email.error}
        autoCapitalize="none"
        autoCompleteType="email"
        textContentType="emailAddress"
        keyboardType="email-address"
        description="Vous recevrez un lien de réinitialisation de votre mot de passe par mail."
      />
      <Button
          mode="contained"
          onPress={sendResetPasswordEmail}
          style={{ marginTop: 16 }}
          label="Recevoir le lien"
          loading={loading}
      />
    </Background>
  )
}
