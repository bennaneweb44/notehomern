declare module 'UserModel' {

  export interface IUser {
    id: number
    email: string
    username: string
    roles: Array
    avatar: string
  }
}
