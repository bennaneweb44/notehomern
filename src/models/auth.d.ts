declare module 'AuthModel' {
  export interface Auth {
    message: string
    token: string
    id: number
  }

  export interface IAuth {
    id: number
    token: string|null
  }
}
