import { IconButton } from 'react-native-paper'
import { StyleSheet, TouchableOpacity, View } from 'react-native'
import { theme } from '@/core/theme'

export default function Disconnect({ onSignOutPressed }: any) {

  return (
    <View style={styles.iconContainer}>
      <TouchableOpacity onPress={onSignOutPressed}>
        <IconButton
          icon="account-arrow-right"
          iconColor={theme.colors.secondary}
          size={36}
        />
      </TouchableOpacity>
    </View>
  )
}

const styles = StyleSheet.create({
  iconContainer: {
    position: 'absolute',
    top: 20,
    right: -10,
  },
})
