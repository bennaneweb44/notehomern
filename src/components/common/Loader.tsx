import { ILoader } from '@/types/app.interfaces';
import SpinnerOverlay from 'react-native-loading-spinner-overlay';

export const Loader = ({ ...props }: ILoader) => {
  const { size, overlayColor, textContent } = props;

  return (
    <SpinnerOverlay
      visible={true}
      size={size}
      overlayColor={overlayColor}
      textContent={textContent}
      textStyle={{ color: '#777' }}
      color='#777'
    />
  )
}
