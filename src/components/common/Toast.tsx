import { useToast } from 'react-native-toast-notifications';
import { IToast } from '@/types/app.interfaces';

export const Toast = ({ message, type }: IToast) => {
  const toast = useToast()

  toast.show(message, {
    type: type,
    placement: 'bottom',
    duration: 3000,
    animationType: 'zoom-in',
  })

  return (
    <></>
  )
}
