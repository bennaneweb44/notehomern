import React, { useEffect, useState } from 'react';
import { FlatList, View } from 'react-native';
import { styles } from './styles';
import ItemScreen from '@/screens/categories/Item/ItemScreen';
import { Text } from 'react-native';

const ListCategories = ({ ...props }) => {
  const { categories } = props;
  const [data, setData] = useState([]);

  useEffect(() => {
    setData(categories);
  }, [categories]);

  return (
    <>
      <View style={styles.container}>
        <Text style={styles.title}>Catégories</Text>
        <FlatList
          style={styles.list}
          contentContainerStyle={styles.listContainer}
          data={data}
          horizontal={false}
          numColumns={2}
          keyExtractor={(item: any) => {
            return item.id
          }}
          renderItem={({ item }: any) => <ItemScreen item={item} />}
        />
      </View>
    </>
  )
}

export default ListCategories;
