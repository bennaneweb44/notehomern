import Button from "@/components/ui/Button"
import { IconButton } from 'react-native-paper'
import { Text, TouchableOpacity } from 'react-native'
import TextInput from '@/components/ui/TextInput'
import { theme } from '@/core/theme'
import { Image } from 'react-native'

export default function ProfilForm({ ...props }: any) {
  const {
    avatar,
    loading,
    onLoadStart,
    onLoadEnd,
    pickImageAsync,
    onProfilUpdated,
    username,
    email,
    onChangeUsername,
    onChangeEmail } = props;

  return (
    <>
      {!avatar ? <Text>Chargement ...</Text> :
        <Image
          source={{ uri: avatar }}
          style={{ width: 200, height: 200, borderRadius: 120, borderWidth: 1, borderColor: theme.colors.primary }}
          onLoadStart={onLoadStart}
          onLoadEnd={onLoadEnd}
        />
      }

      {/* Image button */}
      <TouchableOpacity onPress={pickImageAsync}>
        <IconButton
          mode='contained-tonal'
          icon="camera"
          iconColor={theme.colors.primary}
          size={26}
        />
      </TouchableOpacity>

      {/* Nom d'utilisateur */}
      <TextInput
        label="Nom d'utilisateur"
        returnKeyType="next"
        value={username.value}
        onChangeText={onChangeUsername}
        error={!!username.error}
        errorText={username.error}
        autoCapitalize="none"
        autoCompleteType="text"
        textContentType="username"
        keyboardType="default"
      />

      {/* Email */}
      <TextInput
        label="Email"
        returnKeyType="next"
        value={email.value}
        onChangeText={onChangeEmail}
        error={!!email.error}
        errorText={email.error}
        autoCapitalize="none"
        autoCompleteType="email"
        textContentType="emailAddress"
        keyboardType="email-address"
      />

      {/* Update button */}
      <Button
        mode="contained"
        onPress={onProfilUpdated}
        style={{ marginTop: 10 }}
        label="Mettre à jour"
        loading={loading}
      />
    </>
  )
}
