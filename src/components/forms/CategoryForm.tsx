import { FlatList, SafeAreaView, TouchableOpacity, View, StyleSheet } from 'react-native';
import Button from '@/components/ui/Button';
import TextInput from '@/components/ui/TextInput';
import { useState } from 'react';
import { Icon } from '@/components/ui/Icon';
import FA from '@/common/FontAwesomeIcons.json';
import ColorPicker from 'react-native-wheel-color-picker';

export default function CategoryForm({ ...props }) {
  const { item, setShow, updateCategory } = props;

  const [selectedNom, setSelectedNom] = useState<string>(item.nom);
  const [selectedCouleur, setSelectedCouleur] = useState<string>(item.couleur);
  const [selectedIcone, setSelectedIcone] = useState<string>(item.icone);

  const updateSelectedIcone = (elem: any) => {
    if (elem.item !== selectedIcone) {
      setSelectedIcone(elem.item);
    }
  }

  const renderItem = (elem: any) => (
    <TouchableOpacity onPress={() => updateSelectedIcone(elem)}>
      <Icon name={elem.item} key={elem.index} size={30} color={elem.item === selectedIcone ? selectedCouleur : 'black'} style={styles.faItem} />
    </TouchableOpacity>
  );

  return (
    <View style={styles.globalView}>
      <TextInput
        value={selectedNom}
        onChangeText={(value: string) => setSelectedNom(value)}
        placeholder={'Nom'}
      />
      <SafeAreaView>
        <ColorPicker
          color={selectedCouleur}
          onColorChange={(color) => setSelectedCouleur(color)}
          thumbSize={30}
          sliderSize={30}
          noSnap={true}
          row={false}
        />
      </SafeAreaView>
      
      <Icon name={selectedIcone} size={70} color={selectedCouleur} style={styles.colorIcon} />
      <FlatList
        data={FA}
        renderItem={(elem) => renderItem(elem)}
        keyExtractor={(item) => item}
        numColumns={7}
      />
      <View style={{ flexDirection: 'row' }}>
        <Button
          mode="outlined"
          onPress={() => setShow(false)}
          label="Annuler"
          style={styles.cancelButton}
        />
        <Button
          mode="contained"
          onPress={() => updateCategory({
            id: item.id,
            couleur: selectedCouleur,
            icone: selectedIcone,
            nom: selectedNom
          })}
          label="Mettre à jour"
          style={styles.updateButton}
        />
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  cancelButton: {
    width: '48%',
    flexDirection: 'row',
    justifyContent: 'center',
  },
  updateButton: {
    width: '48%',
    flexDirection: 'row',
    justifyContent: 'center',
  },
  colorIcon: {
    marginTop: 300,
    padding: 5,
    height: 100,
    width: 100,
    alignSelf: 'center',
  },
  globalView: {
    flex: 1,
    backgroundColor: 'whitesmoke',
    padding: 2,
    height: '27%'
  },
  faItem: {
    padding: 5,
    height: 50,
    width: 50,
    alignSelf: 'center',
  },
});
