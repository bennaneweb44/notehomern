import { useState } from "react"
import {
    StyleSheet,
    TouchableOpacity,
    View
} from "react-native"
import { Text } from "react-native-paper"
import Button from "@/components/ui/Button"
import Header from "@/components/ui/Header"
import TextInput from "@/components/ui/TextInput"
import { theme } from "@/core/theme"
// Helpers
import { passwordValidator } from '@/helpers/validators/passwordValidator'
import { nameValidator } from "@/helpers/validators/nameValidator"
import { defaultAuthObject } from "@/common/Constants"

export default function LoginForm({ onLoginPressed, navigation, loading }: any) {
    //const navigation = useNavigation();
    const [username, setUsername] = useState(defaultAuthObject)
    const [password, setPassword] = useState(defaultAuthObject)

    const checkValuesAndSend = async () => {
        const usernameError = nameValidator(username.value)
        const passwordError = passwordValidator(password.value)

        if (usernameError || passwordError) {
            setUsername({ ...username, error: usernameError })
            setPassword({ ...password, error: passwordError })
            return
        }

        onLoginPressed(username.value, password.value)
    }

    return (
        <>
            <Header>🔓 Connexion</Header>
            <TextInput
                label="Nom d'utilisateur"
                returnKeyType="next"
                value={username.value}
                onChangeText={(text: string) => setUsername({ value: text, error: '' })}
                error={!!username.error}
                errorText={username.error}
                autoCapitalize="none"
                autoCompleteType="text"
                textContentType="username"
                keyboardType="default"
            />
            <TextInput
                label="Mot de passe"
                returnKeyType="done"
                value={password.value}
                onChangeText={(text: string) => setPassword({ value: text, error: '' })}
                error={!!password.error}
                errorText={password.error}
                secureTextEntry
            />
            <View style={styles.forgotPassword}>
                <TouchableOpacity
                    onPress={() => navigation.navigate('ResetPasswordScreen')}
                >
                <Text style={styles.forgot}>Mot de passe oublié ?</Text>
                </TouchableOpacity>
            </View>
            <Button
                mode="contained"
                onPress={checkValuesAndSend}
                style={{ marginTop: 10 }}
                label="Se connecter"
                loading={loading}
            />
            <View style={styles.row}>
                <Text>Pas encore de compte ? </Text>
                <TouchableOpacity onPress={() => navigation.navigate('Inscription')}>
                <Text style={styles.link}>S'inscrire</Text>
                </TouchableOpacity>
            </View>
        </>
    )
}

const styles = StyleSheet.create({
    forgotPassword: {
        width: '100%',
        alignItems: 'flex-end',
        marginBottom: -4,
    },
    row: {
        flexDirection: 'row',
        marginBottom: -50,
    },
    forgot: {
        fontSize: 13,
        color: theme.colors.secondary,
    },
    link: {
        fontWeight: 'bold',
        color: theme.colors.primary,
    },
})
