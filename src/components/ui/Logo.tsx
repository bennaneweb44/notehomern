import React from 'react'
import { Text, StyleSheet } from 'react-native'

export default function Logo() {
  return <Text style={styles.image}>✒️</Text>
}

const styles = StyleSheet.create({
  image: {
    fontSize: 40,
    textAlign: 'center',
    paddingTop: 15,
    paddingBottom: -10
  },
})
