import React from 'react';
import { IAppIcons } from '@/types/app.interfaces';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

export const Icon = ({ ...props }: IAppIcons) => (
  <FontAwesome
    name={props.name}
    size={props.size}
    color={props.color}
    style={props.style}
  />
)
