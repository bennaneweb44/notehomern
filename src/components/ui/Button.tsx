import React from 'react'
import { StyleSheet, View } from 'react-native'
import { Button as PaperButton, Text } from 'react-native-paper'
import { theme } from '../../core/theme'
import Spinner from 'react-native-loading-spinner-overlay'


export default function Button({ mode, label, style = {}, loading = false, ...props }: any) {
  return (
    <>
      <PaperButton
        style={[
          styles.button,
          mode === 'outlined' && { backgroundColor: theme.colors.surface },
          style,
        ]}
        labelStyle={styles.text}
        mode={mode}
        disabled={loading === true}
        {...props}
      >
        { label }
      </PaperButton>
      {loading && <Spinner visible={true} animation='fade' /> }
    </>
  )
}

const styles = StyleSheet.create({
  button: {
    width: '100%',
    marginVertical: 6,
    borderRadius: 0,
    borderColor: '#560CCE',
    height: 40
  },
  text: {
    fontWeight: 'bold',
    fontSize: 15,
    lineHeight: 20,
  },
})
