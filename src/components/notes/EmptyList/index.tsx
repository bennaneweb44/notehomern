import { Image, Text, View } from "react-native";
import { styles } from "./styles";
import clipboard from "@/assets/Clipboard.png";

export function EmptyList() {
  return (
    <View style={styles.container}>
      <Image source={clipboard} />
      <Text style={styles.text}>
        Vous n'avez pas encore de tâches enregistrées. {"\n"} Créer des tâches et organiser vos tâches à faire.   
      </Text>
    </View>
  );
}
